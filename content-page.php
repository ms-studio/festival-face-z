<?php
/**
 * The template used for displaying page content.
 *
 * @package Moka
 * @since Moka 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php if ( !is_front_page() ) : ?>
	<header class="entry-header">
	<?php 
		
		// echo '<p>(test parent > enfant)</p>';
		// test if we have a parent:
		
		$subpage_class = '';
		
		if ( is_page() && $post->post_parent ) {
		    // This is a subpage
		    
		    $subpage_class = " has-subpage";
				
				$parent_link = get_permalink($post->post_parent); 
				$parent_title = get_the_title($post->post_parent); 
					
					$parents = get_post_ancestors( $post->ID );
					        /* Get the top Level page->ID count base 1, array base 0 so -1 */ 
						$root_id = ($parents) ? $parents[count($parents)-1]: $post->ID;
						/* Get the parent and set the $class with the page slug (post_name) */
					  $root_parent = get_post( $root_id );
						$root_title = $root_parent->post_title;
						$root_link = get_permalink( $root_id );
				
				echo '<div class="parent-page"><span class="parent-page-root">';
				
				echo '<a href="'. $root_link .'">'. $root_title .'</a></span> &gt; ';
				
				echo '<a href="'. $parent_link .'">'. $parent_title .'</a></div>';
				
		}
	
	 ?>		
		<h1 class="entry-title<?php echo $subpage_class; ?>"><?php the_title(); ?></h1>
	</header><!-- end .entry-header -->
	<?php endif; ?>

	<div class="entry-content clearfix">
		<?php the_content(); ?>
	</div><!-- end .entry-content -->

</article><!-- end post-<?php the_ID(); ?> -->