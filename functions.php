<?php 

// Change-Detector-XXXXXXX - for Espresso.app

/* Allow Automatic Updates
 ******************************
 * http://codex.wordpress.org/Configuring_Automatic_Background_Updates
 */

add_filter( 'auto_update_plugin', '__return_true' );
add_filter( 'auto_update_theme', '__return_true' );
add_filter( 'allow_major_auto_core_updates', '__return_true' );


add_action( 'after_setup_theme', 'my_child_theme_setup' );
function my_child_theme_setup() {
    
    load_theme_textdomain( 'moka', get_stylesheet_directory() . '/lang' );
    
    // note: thumbnails in MOKA:
    // add_image_size( 'recentposts-widget-img', 300, 300, true );
    // add_image_size( 'recent-img', 360, 480, true );
    // add_image_size( 'slider-img', 1070, 600, true );
    add_image_size( 'poster-img', 300, 425, true );
    
    // for Mailpoet:
    // https://wordpress.org/support/topic/changing-the-size-of-images-in-automatic-created-emails
    add_image_size( 'single-post-thumbnail', 600, 600, false );
        
}


/*
 * Custom Sizes
 **************************
*/



/* Widget Areas
******************************/

register_sidebar( array(
		'name'          => 'Pied de menu',
		'id'            => 'menu-bottom',
		'description'   => 'Zone éditable en pied de menu.',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
) );



function custom_register_styles() {

	/**
	 * Custom CSS
	 */

	// the MAIN stylesheet
	
	
	if ( WP_DEBUG == true ) {
	
			// DEV: the MAIN stylesheet - uncompressed
			wp_enqueue_style(
					'main-style',
					get_stylesheet_directory_uri() . '/css/dev/00-main.css', // main.css
					false, // dependencies
					null // version
			); 
	
	} else {
	
			// PROD: the MAIN stylesheet - combined and minified
			wp_enqueue_style( 
					'main-style', 
					get_stylesheet_directory_uri() . '/css/build/styles.20151110150908.css', // main.css
					false, // dependencies
					null // version
			); 
	}

	wp_dequeue_style( 'moka-style' );
	wp_dequeue_style( 'moka-flex-slider-style' );
	
	wp_dequeue_style( 'moka-lato' );
	
	wp_dequeue_script( 'moka-flex-slider' );
	
	wp_enqueue_script( 
	// the MAIN JavaScript file -- development version
			'main-script',
			get_stylesheet_directory_uri() . '/js/scripts.js', // scripts.js
			array('jquery'), // dependencies
			null, // version
			true // in footer
	);

}

add_action( 'wp_enqueue_scripts', 'custom_register_styles', 21 );


/* admin interface
******************************/

if ( is_user_logged_in() ) {
		require_once('functions-admin.php');
}


/* Allowed FileTypes
 ********************
 * method based on 
 * http://howto.blbosti.com/?p=329
 * List of defaults: https://core.trac.wordpress.org/browser/tags/3.8.1/src/wp-includes/functions.php#L1948
*/

add_filter('upload_mimes', 'custom_upload_mimes');
function custom_upload_mimes ( $existing_mimes=array() ) {

		// add your extension to the array
		$existing_mimes['svg'] = 'image/svg+xml';

		// removing existing file types
		unset( $existing_mimes['bmp'] );
		unset( $existing_mimes['tif|tiff'] );

		// and return the new full result
		return $existing_mimes;
}


/*
 * File Upload Security
 
 * Sources: 
 * http://www.geekpress.fr/wordpress/astuce/suppression-accents-media-1903/
 * https://gist.github.com/herewithme/7704370
 
 * See also Ticket #22363
 * https://core.trac.wordpress.org/ticket/22363
 * and #24661 - remove_accents is not removing combining accents
 * https://core.trac.wordpress.org/ticket/24661
*/ 

add_filter( 'sanitize_file_name', 'remove_accents', 10, 1 );
add_filter( 'sanitize_file_name_chars', 'sanitize_file_name_chars', 10, 1 );
 
function sanitize_file_name_chars( $special_chars = array() ) {
	$special_chars = array_merge( array( '’', '‘', '“', '”', '«', '»', '‹', '›', '—', 'æ', 'œ', '€','é','à','ç','ä','ö','ü','ï','û','ô','è' ), $special_chars );
	return $special_chars;
}



