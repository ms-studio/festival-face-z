<?php
/**
 * Template Name: Face-Z Vignettes
 * Description: Une page vignettes (montre les pages-enfants)
 *
 * @package Face-Z
 */

get_header(); ?>

	<div id="primary" class="site-content" role="main">

		<?php
		
		
			// Start the Loop.
			while ( have_posts() ) : the_post();

				// Include the page content template.
				// get_template_part( 'content', 'page' );
				?>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				
					<header class="entry-header">
						<h1 class="entry-title vignettes-title"><?php the_title(); ?></h1>
					</header><!-- end .entry-header -->
														
					<div class="entry-content clearfix">
						<?php 
						
						
						// Make a query for child pages.
									
									$current_page_id = get_the_ID();
									
									$list_query = new WP_Query( array(
											 	'posts_per_page' => -1,
											 	'post_type' => 'page',
											 	'post_parent' => $current_page_id,
											 	'orderby' => 'menu_order',
						  					'order' => 'ASC',
											 	) ); 
											 	
											 	if ($list_query->have_posts()) : 
											 
								  			 			  			 	         
								  			 while( $list_query->have_posts() ) : $list_query->the_post();
								  			 
								  			 	// include( get_stylesheet_directory() . '/inc/archive-item.php' );
						//  		  			 	get_template_part( 'inc/archive-item', '' );
						
															?>
															<div class="vignette">
																	<a href="<?php the_permalink() ?>" class="dblock">
																	
																<?php 
																
																if ( has_post_thumbnail() ) {
																	the_post_thumbnail( 'recentposts-widget-img' );
																}
																else {
																	echo '<img src="' . get_stylesheet_directory_uri() . '/img/pixels-300.png" />';
																}
																
																 ?>
																	  
																	    <h3 class="vignette-title" id="post-<?php the_ID(); ?>"><?php the_title(); 
																	    
																	     ?>
												    		    </h3>
												    			</a>
												    	 </div>
												    <?php
								  			 
								  			 endwhile; 
								  			 
											endif;
									wp_reset_postdata();
								
						
						
						
						the_content(); ?>
					</div><!-- end .entry-content -->
				
				</article><!-- end post-<?php the_ID(); ?> -->
			
			<?php
			
			endwhile;
		?>

	</div><!-- end #primary -->

<?php get_footer(); ?>